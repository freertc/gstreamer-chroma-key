#!/bin/bash
set -e

REAL_WEBCAM="v4l2src device=/dev/video0"

OUTPUT_VIRT_WEBCAM="v4l2sink device=/dev/video2"

#W=1920
#H=1080
W=1280
H=720
FPS=24


# use standard green background curtain
ALPHA_PARAMS="method=green"
# use a custom green
#ALPHA_PARAMS="method=custom target-r=73 target-g=112 target-b=81"
# use a custom blue
#ALPHA_PARAMS="method=custom target-r=122 target-g=81 target-b=111"

SRC_PATTERN="videotestsrc pattern=1 ! video/x-raw,format=I420,framerate=(fraction)10/1,width=${W},height=${H}"
SRC_IMAGE="multifilesrc location=background1.jpg caps=image/jpeg,framerate=${FPS}/1 ! jpegdec ! videoscale ! video/x-raw,format=I420,width=${W},height=${H}"

BACKGROUND=${SRC_IMAGE}

gst-launch-1.0 ${BACKGROUND} ! \
    compositor name=comp sink_0::alpha=1 sink_1::alpha=1 ! \
    videoconvert ! \
    ${OUTPUT_VIRT_WEBCAM} \
    ${REAL_WEBCAM} ! \
    video/x-h264,width=${W},height=${H},framerate=${FPS}/1 ! \
    h264parse ! avdec_h264 ! \
    alpha ${ALPHA_PARAMS} ! \
    videoconvert ! comp.

