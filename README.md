
This is a basic script to take the input from a webcam, do chroma keying
with gstreamer and make the modified video available to other applications
as a virtual webcam.


Diagram
-------

Here is an architecture diagram, assuming that you are using
Mozilla Firefox for WebRTC video calls:

```
Real webcam     ---> gstreamer-virtual-webcam -> v4l2loopback -> Firefox
                      (this script)              /dev/video2
Extra image             |
(photo, another --------+
video feed or
a video file
on disk)
```


Limits
------

In Firefox, some WebRTC sites don't see the virtual webcam but others are
working.
Working: JsSIP, JSCommunicator, FedRTC.org, Freephonebox.net, Skype
Not working: Zoom, Jitsi Meet
See this bug report for details:
https://bugzilla.mozilla.org/show_bug.cgi?id=1622628

In recent Chrome versions on recent Linux kernels, the virtual webcam
doesn't work for any sites, it doesn't even appear in the preferences
page at chrome://setings/content/camera
https://bugs.chromium.org/p/chromium/issues/detail?id=757399


Setup
-----

Install packages that are essential or useful for us and make
sure you have permission to use the video devices:

```
sudo apt install v4l-utils v4l2loopback-dkms v4l2loopback-utils
sudo adduser ${USER} video
```

Load the kernel module.  It is essential to enable `exclusive_caps=1`
for some applications.

```
sudo modprobe v4l2loopback exclusive_caps=1 card_label="VirtualWebCam"
```

Each time you boot your system or load the module, you need to
run this command at least once, using the resolution that you prefer
and the correct video loopback device:

```
v4l2loopback-ctl set-caps "video/x-raw,format=UYVY,width=1280,height=720,framerate=25" /dev/video2
```

Running and testing it
----------------------

Check the names of your webcam and loopback video device nodes:

```
ls -l /dev/video*
```

Look inside the script, you probably need to adjust the video device
nodes (e.g. /dev/video0, /dev/video2) to match your system.

To run the script, execute it in a terminal window and then leave the
window open:

```
./gstreamer-virtual-webcam.sh
```

Now you can go to another terminal and test it with guvcview.  If
your loopback device node is /dev/video2, then you can use this:

```
guvcview -d /dev/video2
```

Troubleshooting
---------------

You can inspect each video device node like this:

```
$ v4l2-ctl -D -d /dev/video2 
Driver Info:
	Driver name      : v4l2 loopback
	Card type        : VirtualWebCam
	Bus info         : platform:v4l2loopback-000
	Driver version   : 4.19.98
	Capabilities     : 0x85208003
		Video Capture
		Video Output
		Video Memory-to-Memory
		Read/Write
		Streaming
		Extended Pix Format
		Device Capabilities
	Device Caps      : 0x85208003
		Video Capture
		Video Output
		Video Memory-to-Memory
		Read/Write
		Streaming
		Extended Pix Format
		Device Capabilities
```

Notice that the device is in Video Output mode, this means the
gstreamer script is not running or the exclusive_caps=1 argument
was not passed to the v4l2loopback module.

When the exclusive_caps=1 has been specified and the gstreamer
script is running, Video Output should not appear in the output
of this command.

In Chromium and Chrome, you can check chrome://settings/content/camera to
see if it detected the virtual webcam.

Links, getting help
-------------------

https://lists.freedesktop.org/archives/gstreamer-devel/
https://github.com/umlaeute/v4l2loopback

Credits
-------

The sample background picture is St Kilda Beach, Melbourne, Australia
from Wikimedia Commons
https://commons.wikimedia.org/wiki/File:Melbourne_(AU),_St_Kilda_Beach_--_2019_--_1590.jpg

